function result = dtwknn(train, labels, x, K)
    %Calculate d(x, xi) i =1, 2, .., n; where d denotes the DTW distance between the points.
    N = numel(train); % train is a structure, each element correspnods to feature vector of variable size
    distances = zeros(1, N);
    for i=1:N
        feature = train{i};
        distances(i) = dtw(feature, x);
    end
    %Arrange the calculated n Euclidean distances in non-decreasing order.
    [~, dist_order] = sort(distances);
    featuresSorted = train(dist_order);
    labelsSorted = labels(dist_order);
    %Let k be a +ve integer, take the first k distances from this sorted list.
    %Find those k-points corresponding to these k-distances.
    firstKSorted = featuresSorted(1:K);
    %Let ki denotes the number of points belonging to the ith class among k points i.e. k ? 0
    myks = zeros(1,K);
    for i=1:K
        label = labelsSorted(i);
        myks(label) = myks(label) + 1;
    end
    %If ki >kj ? i ? j then put x in class i.
    [~, result] = max(myks);
    if (result ~= x(1))
        shit= 0;
    end