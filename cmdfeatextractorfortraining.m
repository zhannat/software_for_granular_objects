train=[];
test =[];
for i=1:15
    featF = featuresOfCmdVel1ByFingers{i};
    for j=1:5
        feat = featF{j};
        len = size(feat ,1);
        % 3 7
        shift = floor(len/10);
        test1 = feat(shift*2:shift*3-1,1);
        test2 = feat(shift*6:shift*7-1);
        train1 = feat(1:shift*2-1);
        train2 = feat(shift*3:shift*6-1);
        train3 = feat(shift*7:end);
        test = [test;test1;test2;];
        train =[train;train1;train2;train3;];
    end
end

a = size(train, 1);  
    % Shuffle data 
    rand_ind = randperm(size(train, 1));
    B = train(rand_ind, :);
    B = cell2mat(B);
    
    % Normalize data to (0, 1) values
    maxs = max(B(:, 1:end-1));
    mins = min(B(:, 1:end-1));

    for ind=1:size(maxs,2)
        if maxs(ind) == mins(ind)
            mins(ind) = maxs(ind) - 1;
        end
    end
    C = B;
    for ind = 1:size(B, 1) 
        C(ind, :) = [(B(ind, 1:end-1) - mins) ./ (maxs - mins), B(ind, end)];
    end 
    train = C;
    
    testing = test;
    clearvars test;
        
    testing = cell2mat(testing);
    CTEST1 = testing;
    for ind = 1:size(testing, 1)
        CTEST1(ind, :) = [(testing(ind, 1:end-1) - mins) ./ (maxs - mins), testing(ind, end)];
    end
    testing = CTEST1;
    clearvars CTEST1;