trainF1=[];
testF1 =[];
trainF2=[];
testF2 =[];
featlabelsTrain = [];
featlabelsTest = [];
counter1 = 0;
counter2 = 0;
for i=1:15
    featF = featuresOfElectrodesVel1ByFingers{i};
    for j=1:5
        feat = featF{j};
        feat1 = feat{1};
        feat2 = feat{2};
        len = size(feat1,1);
        % 3 7
        shift = floor(len/10);
        test1 = feat1(shift*2:shift*3-1,1);
        test2 = feat1(shift*6:shift*7-1);
        train1 = feat1(1:shift*2-1);
        train2 = feat1(shift*3:shift*6-1);
        train3 = feat1(shift*7:end);
        testF1 = [testF1;test1;test2;];
        trainF1 =[trainF1;train1;train2;train3;];
        
        shift = floor(len/10);
        test1 = feat2(shift*2:shift*3-1,1);
        test2 = feat2(shift*6:shift*7-1);
        train1 = feat2(1:shift*2-1);
        train2 = feat2(shift*3:shift*6-1);
        train3 = feat2(shift*7:end);
        testF2 = [testF2;test1;test2;];
        trainF2 =[trainF2;train1;train2;train3;];
        
        for k=1:size(test1,1)+size(test2,1)
            counter1 = counter1 + 1;
            featlabelsTest(counter1) = i; 
        end
        
        for k=1:size(train1,1)+size(train2,1)+size(train3,1)
            counter2 = counter2 + 1; 
            featlabelsTrain(counter2) = i;
        end
    end
end


labelsstr = unique(featlabelsTrain);
myperm = randperm(numel(featlabelsTrain));

shuflabels = featlabelsTrain(myperm);
shufdata1 = trainF1(myperm);
shufdata2 = trainF2(myperm); 
shuflabelsnum = zeros(1, numel(featlabelsTrain));
for i=1:numel(shuflabels)  
    shuflabelsnum(i) = shuflabels(i);
end
 
traindata1 = shufdata1;
traindata2 = shufdata2;
trainlabels = shuflabelsnum;

testdata1 = testF1;
testdata2 = testF2;
testlabels = featlabelsTest;

% store predictions of testing data in the following variable
Mtst = numel(testlabels);
preds1 = zeros(1, Mtst);
preds2 = zeros(1, Mtst); 
K = numel(labelsstr);
for i=1:Mtst  
    pred1 = dtwknn(traindata1, trainlabels, testdata1{i}, K);
    preds1(i) = pred1;
    
    pred2 = dtwknn(traindata2, trainlabels, testdata2{i}, K);
    preds2(i) = pred2; 
end
% calculate the accuracy
acc1 = sum(preds1 == testlabels) / numel(preds1);
acc2 = sum(preds2 == testlabels) / numel(preds2); 
disp(acc1);
disp(acc2); 

%%
Mtst = numel(traindata1);
preds1 = zeros(1, Mtst);
preds2 = zeros(1, Mtst);
preds3 = zeros(1, Mtst);
K = numel(labelsstr);
for i=1:Mtst  
    pred1 = dtwknn(traindata1, trainlabels, traindata1{i}, K);
    preds1(i) = pred1;
    
    pred2 = dtwknn(traindata2, trainlabels, traindata2{i}, K);
    preds2(i) = pred2;
     
end
% calculate the accuracy
acc1 = sum(preds1 == trainlabels) / numel(preds1);
acc2 = sum(preds2 == trainlabels) / numel(preds2); 
disp(acc1);
disp(acc2); 